__author__ = 'SANTECH'

import os
from bs4 import BeautifulSoup
import json

update = False

if update == True:

    with open(os.path.join(os.getcwd(), "static", "dataset.xml")) as lista_dataset_xml:
        lista_dataset = lista_dataset_xml.read()

    lista_dataset_parsed =BeautifulSoup(lista_dataset  , 'xml')

    dizionario_dataset = {}

    for item in lista_dataset_parsed.findAll("Dataflow"):
        dizionario_dataset[item["id"]] = item.findAll("Name", {"xml:lang":"en"})[0].string

    with open('dataset_eurostat.json', 'w') as outfile:
        json.dump(dizionario_dataset, outfile)


with open(os.path.join(os.getcwd(), "dataset_eurostat.json")) as lista_dataset_xml:
    lista_dataset = json.loads(lista_dataset_xml.read())


print len(lista_dataset.keys())