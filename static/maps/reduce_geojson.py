import json
import os


def nuts_italiani():
    with open("nuts2.json") as nuts2:
        dict_map = json.loads(nuts2.read())

    dict_ita = {u'type': u'FeatureCollection', u'features':[]}


    for nuts in dict_map[u'features']:
        if nuts[u'properties'][u'NUTS_ID'][0:2] == "IT":
            dict_ita[u'features'].append(nuts)


    print dict_ita

    with open("nuts2_ita.json", "w") as nuts2_ita:
        nuts2_ita.write(json.dumps(dict_ita))

def solo_nuts3():
    with open("nuts3.geojson") as nuts3:
        dict_map = json.loads(nuts3.read())

    dict_nuts3 = {u'type': u'FeatureCollection', u'features':[]}


    for nuts in dict_map[u'features']:
        if len(nuts[u'properties'][u'NUTS_ID']) == 5:
            dict_nuts3[u'features'].append(nuts)


    print dict_nuts3

    with open("nuts3.geojson", "w") as nuts3:
        nuts3.write(json.dumps(dict_nuts3))


def solo_nuts3_2010():
    with open("nuts3_2010.geojson") as nuts3:
        dict_map = json.loads(nuts3.read())

    dict_nuts3 = {u'type': u'FeatureCollection', u'features':[]}


    for nuts in dict_map[u'features']:
        if len(nuts[u'properties'][u'NUTS_ID']) == 5:
            dict_nuts3[u'features'].append(nuts)


    print dict_nuts3

    with open("nuts3_2010.geojson", "w") as nuts3:
        nuts3.write(json.dumps(dict_nuts3))


solo_nuts3_2010()