__author__ = 'SANTECH'


from flask import Flask, request, render_template, redirect, url_for, send_from_directory
from flask.ext.bootstrap import Bootstrap

import os
import json

from sdmx_import import importa_singolo
from sdmx_import import codici_sdmx
from riduzione import Riduzione
from map import map_join
import random


from sys import platform as _platform
if _platform == "linux" or _platform == "linux2":
    linux = True
elif _platform == "darwin":
    linux = True
elif _platform == "win32":
    linux = False



if linux == True:
    os.chdir(os.path.join(os.getcwd(), "eurostat_fvg"))


with open(os.path.join(os.getcwd(), "dataset_eurostat.json")) as lista_dataset_xml:
        lista_dataset = json.loads(lista_dataset_xml.read())

class globali():
    def __init__(self):
        self.risultati = False
        self.dizionario = False

globale = globali()



app = Flask(__name__)
bootstrap = Bootstrap(app)




@app.route("/")
def indice():
    dataset_presenti = []
    with open(os.path.join(os.getcwd(), "rimossi.txt"), 'r') as file_rimossi:
        quarantena = file_rimossi.read()
    quarantena = json.loads(quarantena)
    for item in os.listdir(os.path.join(os.getcwd(), "static", "tsv")):
        if item[-3:] == "tsv":
            if item[:-4] in quarantena["items"]:
                pass
            else:
                dataset_presenti.append(item[:-4])


    return render_template("index.html", dataset_presenti = dataset_presenti, lista_dataset = lista_dataset)

@app.route("/esplora")
def esplora():
    lista_dataset
    return render_template("lista_dataset.html", lista_dataset = lista_dataset)

@app.route("/aggiungi", methods=['POST'])
def aggiungi():

    dataset = request.form["dataset"]
    importa_singolo(dataset)
    return "eseguito"


@app.route("/riduci", methods=['POST'])
def riduci():
    #dataset = request.form["dataset"]


    datasets = request.form.getlist("dataset")
    print datasets
    globale.datasets = {}

    dizionario_dataset = {}


    for codice in datasets:
        dizionario_dataset[codice] = {}
        titolo =  lista_dataset[codice]
        codici_variabili = codici_sdmx(os.path.join(os.getcwd(), "static", "dsd", codice + ".dsd.xml"))
        dizionario_dataset[codice]["titolo"] = titolo
        dizionario_dataset[codice]["codici_variabili"] = codici_variabili


    return render_template("dimensioni.html", dizionario_dataset = dizionario_dataset, lista_dataset = lista_dataset)

@app.route("/rimuovi/<item>")
def rimuovi(item):
    #os.remove(os.path.join(os.getcwd(), "static", "tsv", item + ".tsv"))

    return render_template("rimuovi.html", item = item)

@app.route("/funzione_rimuovi/<item>", methods=['POST'])
def funzione_rimuovi(item):
    if request.form["password"] == "pollo":

        with open(os.path.join(os.getcwd(), "rimossi.txt"), 'r') as file_rimossi:
            quarantena = file_rimossi.read()
        #print quarantena
        quarantena = json.loads(quarantena)
        quarantena["items"].append(item)
        with open(os.path.join(os.getcwd(), "rimossi.txt"), 'w') as file_rimossi:
            file_rimossi.write(json.dumps(quarantena))

        #return str(item)
        return "rimosso " + item
    else:
        return "password sbagliata"
@app.route("/ridotto", methods=['POST'])
def ridotto():
    risultato_form = dict(request.form)

    risultati = {}
    for codici in risultato_form.keys():
        codice = codici.split("|")[0]
        risultati[codice] = {}

    for codici in risultato_form.keys():
        codice = codici.split("|")[0]
        variabile = codici.split("|")[1]
        valore = risultato_form[codici]
        risultati[codice][variabile] = valore


    print risultati

    dizionario_risultati ={}
    for codici in risultati.keys():
        risultato = Riduzione(os.path.join(os.getcwd(), "static", "HDF5", codici + ".h5"), dizionario_var_stato = risultati[codici], nome_file = codici)
        dizionario_risultati[codici] = risultato
        globale.risultati = dizionario_risultati



    anni = ["ultimo valore disponibile"]


    return render_template("operazione.html",risultati = dizionario_risultati.keys(), operatore = ["operatore1", "operatore2"], anni = anni)


@app.route("/risultato_operazione", methods=['POST'])
def risultato_operazione():

    risposte = dict(request.form)

    dataset_finale = []
    operatore1 = []
    operatore2 = []
    titolo1 = ""
    titolo2 = ""
    op1 = False
    op2 = False

    for risposta in risposte:
        if risposte[risposta] == [u'solo']:
            titolo1 = lista_dataset[risposta]
            dataset_finale = globale.risultati[risposta]
        elif risposte[risposta] == [u'primo']:
            operatore1 = globale.risultati[risposta]
            titolo1 = lista_dataset[risposta]
            op1 = True
        elif risposte[risposta] == [u'secondo']:
            titolo2 = lista_dataset[risposta]
            operatore2 = globale.risultati[risposta]
            op2 = True
        else:
            print "altro "

    operazione = risposte["operazione"]
    '''
    print "dataset finle"
    print dataset_finale
    print "operatore1"
    print operatore1
    print operatore2
    print operazione
    '''

    if op1 == True and op2 == True:
        if operazione == ["divisione"]:
            dataset_finale = operatore1 / operatore2
            operazione = " / "
        elif operazione == ["moltiplicazione"]:
            dataset_finale = operatore1 * operatore2
            operazione = " * "
        elif operazione == ["somma"]:
            dataset_finale = operatore1 + operatore2
            operazione = " + "
        elif operazione == ["sottrazione"]:
            operazione = " - "
            dataset_finale = operatore1 + operatore2
        elif operazione == None:
            pass
    else:
        operazione = ""




    #print dataset_finale.to_html()

    #recipero tipo di nuts da form.request
    risposta_nuts = risposte["nuts"][0]

    #recupero polarita' da form.request
    polar = risposte["polar"][0]

    #recupero tipo valore da form.request
    anni = risposte["anno"][0]

    moltiplicatore = risposte["moltiplicatore"][0]

    nome_grafico = risposte["nome_grafico"][0]
    #creo un dizionario con i risultati [output]
    output = map_join(dataset_finale, nuts=risposta_nuts, moltiplicatore = float(moltiplicatore))
    mappa = json.dumps(output["json"])
    html = output["html"]
    gradi = output["gradi"]
    excel = output["pandas"]
    geojson_code = random.randint(0, 10000000)
    excel.to_excel(os.path.join(os.getcwd(), "static", "excel_output", str(geojson_code)  + ".xlsx"))
    with open(os.path.join(os.getcwd(), "static", "geojson", str(geojson_code) + ".geojson"), "w") as geojson_file:
        geojson_file.write(mappa)





    return render_template("mappa.html", mappa = mappa, dati_html = html, gradi = gradi , scala = polar,  operatore1 = titolo1, operatore2 = titolo2, operazione = operazione, anni = anni, nome_grafico = nome_grafico, geojson_code = geojson_code, excel_code = geojson_code)


@app.route("/italia")
def italia():
    with open(os.path.join(os.getcwd(), "static",  "italia.json")) as italia_nuts:
        italia = json.loads(italia_nuts.read())
        print type(italia)
    return render_template("inserimento_italia.html", italia = italia)

@app.errorhandler(500)
def page_not_found(e):
    return render_template('500.html'), 500



if __name__ == "__main__":
    app.run(debug=True)