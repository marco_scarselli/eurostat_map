import os
import pandas as pd
import numpy as np

def Riduzione(ref_file, dizionario_var_stato, nome_file = None):

    print ref_file

    if ref_file[-4:] == "xlsx":
        dati = pd.read_excel(ref_file)
    else:
        dati = pd.read_hdf(ref_file, nome_file)

    #print dati
    for key in dizionario_var_stato.keys():
        variabile = key[3:].lower()
        stato = dizionario_var_stato[key][0]
        dati = dati[dati[variabile] == stato]

    dati = dati.dropna(subset=['KeyB'], how='all')
    dati.set_index("KeyB", inplace = True)

    dati = dati.T[dati.columns.map(lambda x: type(x) == int)]

    return dati.T

if __name__ == "__main__":
    ref_file = os.path.join(os.getcwd(), "static", "excel", "crim_gen_reg.xlsx")
    dizionario_var_stato = {'CL_UNIT': [u'NR'], 'CL_ICCS': [u'ICCS0401']}
    print Riduzione(ref_file, dizionario_var_stato)

