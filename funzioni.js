var grades = [1, 7, 12, 18, 25, 30, 37, 'missing'] ;var descrizione = "<h4> output. </h4>";var anno = '2014' ;
        function getColor(d) {
        return d == "Non disponibile" ? '#FFFFFF':
		d > grades[6]   ? '#EB000F':
		d > grades[5]   ? '#FF4341':
		d > grades[4]   ? '#E9A628' :
		d > grades[3]   ? '#DED81C' :
		d > grades[2] ? '#9AD312' :
        d > grades[1] ? '#52C808' :
		d >= grades[0] ? '#0CBD00':
		'#FFFFFF';
        }
        function decimali(numero) {
                return parseFloat(numero).toFixed(2)

                }



                // control that shows state info on hover
                var info = L.control();

                info.onAdd = function (map) {
                this._div = L.DomUtil.create('div', 'info');
                this.update();
                return this._div;
                };


                // cambiare le propriet� da mettere in rilievo  -->  attributi props.

                info.update = function (props, anno) {
                this._div.innerHTML = descrizione +  (props ?
                    '<b>' + props.Nome + '</b><br />'
                    +"classifica-rank ="  + props["rank"] + '</b><br />'
                    + anno + "="  + decimali(props[anno])
                                    // cambiare unit�
                    : 'punta una regione');
                };


                function style(feature) {
                return {
                    weight: 2,
                    opacity: 1,
                    color: 'white',
                    dashArray: '3',
                    fillOpacity: 0.7,
                    fillColor: getColor(feature.properties.rank) // cambiare propriet�
                };
                }



                var legend = L.control({position: 'bottomright'});

                legend.onAdd = function (map) {

                var div = L.DomUtil.create('div', 'info legend'),
                    labels = [],
                    from, to;

                for (var i = 0; i < grades.length - 1; i++) {
                    from = grades[i];
                    to = grades[i + 1];

                    labels.push(
                        '<i style="background:' + getColor(from + 1) + '"></i> ' +
                        from + (to ? '&ndash;' + String(parseInt(to) - 1) : '+'));
                }

                div.innerHTML = labels.join('<br>');
                return div;
                };