# -*- coding: utf-8 -*-
__author__ = 'SANTECH'

import pandas as pd
import numpy as np

def estrai_ultimo_valore_serie(dati):
    dataframe = dati
    dataframe.columns = dataframe.columns.map(lambda x: int(x))

    max = dataframe.columns.max()
    #print max
    min = dataframe.columns.min()
    dataframe["ultimo valore disponibile"] = np.nan
    #print min
    for row in dataframe.index:
        #print str(row) + "----"
        valore_recente = np.nan
        for anno in range(min, max + 1, 1):
            valore = dataframe[anno].loc[row]
            #print valore
            if ~np.isnan(valore):
                valore_recente = valore
            else:
                pass
        dataframe["ultimo valore disponibile"].loc[row] = valore_recente

    return dataframe





if __name__ == "__main__":
    esempio = pd.DataFrame([[20, 70, np.nan] ,[30,50, np.nan] ,[40, np.nan, np.nan]], columns = ["2012","2011","2010"], index = ["it", "uk", "de"])
    print esempio
    #esempio = pd.DataFrame([["Germania", "Italia"],[20, np.nan] ,[30,50] ,[40, np.nan], [u"alto", u"alto"]], index = ["Luogo", 2012,2011,2010, "media"]).T
    print estrai_ultimo_valore_serie(esempio)