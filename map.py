__author__ = 'SANTECH'




import json
import pandas as pd
import os

from estrai_ultimo_valore import estrai_ultimo_valore_serie


from sys import platform as _platform
if _platform == "linux" or _platform == "linux2":
    linux = True
elif _platform == "darwin":
    linux = True
elif _platform == "win32":
    linux = False




if linux == False:
    CodiceMap = pd.read_excel(os.path.join(os.getcwd(),"static", "KEY.xlsx"))
elif linux == True:
    CodiceMap = pd.read_excel(os.path.join(os.getcwd(),"eurostat_fvg", "static", "KEY.xlsx"))
else:
    print "errore caricamento KEY"









def map_join(dataset = None, nuts = "nuts0", anno = False, CodiceMap = CodiceMap, moltiplicatore = 1):
    '''questa liberia serve per applicare i seguenti miglioramenti:
    aggiunta media e rank per la mappa
    join con le coordinate geojson e
    trasformazione in json
    '''

    risultato = {}
    #print type(nuts)
    #print nuts == "nuts0"
    #print "errore " + str(nuts)


    dataset = dataset * moltiplicatore

    dataset = pd.merge(dataset, CodiceMap[["KeyB", "CodiceMAP"]], left_index=True, right_on="KeyB", how = "left")
    if nuts == "nuts0":
        dataset = dataset[dataset["KeyB"] < 35]
    elif nuts == "nuts2":
        dataset = dataset[(dataset["KeyB"] < 10344) & (dataset["KeyB"] > 9999)]
        print "nuts2"
        print dataset
    elif nuts == "nuts3":
        pass
    elif nuts == "Italia nuts2":
        def checkita(x):
            if type(x) == unicode:
                if x[:2] == "IT" and len(x) == 4:
                    return True
                else:
                    return False
            else:
                return False
        dataset = dataset[(dataset["KeyB"] < 10344) & (dataset["KeyB"] > 9999)]
        dataset = dataset[dataset["CodiceMAP"].map(lambda x: checkita(x))]
        dataset.drop_duplicates("CodiceMAP", inplace = True)
        print dataset
    else:
        print "riduzione nuts errore"



    dataset = dataset.drop(["KeyB"], axis = 1)
    dataset.set_index("CodiceMAP", inplace = True)
    dataset = dataset.groupby(dataset.index).max()



    #print "dataset"
    #print dataset

    #aggiungere media


    dataset_ultimo_var = estrai_ultimo_valore_serie(dataset)
    dataset["media"] = dataset.mean(axis = 1)
    dataset_ultimo_var["media"] = dataset["media"]
    dataset = dataset_ultimo_var
    #aggiungere rank


    #aggiungere selezione specifico anno
    if anno == False:

        dataset["rank"] = dataset["ultimo valore disponibile"].rank(method = 'max', ascending = False)
        dataset = dataset.dropna(subset = ["rank"])
        dataset = dataset.sort(["rank"], ascending = True)

    if anno != False:
        dataset["rank"] =  dataset[anno].rank(method = 'max', ascending = False)
        dataset = dataset.dropna(subset = ["rank"])
        dataset = dataset.sort(["rank"], ascending = True)

    n_casi = dataset["rank"].max()
    print "dataset dopo media"
    CodiceMap = CodiceMap.set_index("CodiceMAP")

    dataset = dataset.merge(CodiceMap[["Nome"]], left_index=True, right_index=True, how = "inner")

    dataset = dataset.groupby(dataset.index).max()
    dataset = dataset.sort(["rank"], ascending = True)
    dataset_pandas = dataset
    html = dataset.reset_index().to_html(index="False")
    #si aggiunge i nomi


    dataset = dataset.to_json(orient="index")
    dataset = json.loads(dataset)


    # va aggiunto nuts3 e nuts 3 italia
    if nuts == "nuts0":
        mappa = os.path.join(os.getcwd(), "static", "maps", "nuts0.json")
        #gradi = [1, 7, 12, 18, 25, 30, n_casi, 'missing']
        try:
            gradi = [1, int(n_casi/7*2), int(n_casi/7*3), int(n_casi/7*4), int(n_casi/7*5), int(n_casi/7*6), n_casi, 'missing']
        except:
            n_casi = 35
            gradi = [1, int(n_casi/7*2), int(n_casi/7*3), int(n_casi/7*4), int(n_casi/7*5), int(n_casi/7*6), n_casi, 'missing']
    elif nuts == "nuts2":
        mappa = os.path.join(os.getcwd(), "static", "maps", "nuts2.json")
        try:
            gradi = [1, int(n_casi/7*2), int(n_casi/7*3), int(n_casi/7*4), int(n_casi/7*5), int(n_casi/7*6), n_casi, 'missing']
        except:
            n_casi = 250
            gradi = [1, int(n_casi/7*2), int(n_casi/7*3), int(n_casi/7*4), int(n_casi/7*5), int(n_casi/7*6), n_casi, 'missing']
    elif nuts == "Italia nuts2":
        mappa = os.path.join(os.getcwd(), "static", "maps", "nuts2_ita.json")
        try:
            gradi = [1, int(n_casi/7*2), int(n_casi/7*3), int(n_casi/7*4), int(n_casi/7*5), int(n_casi/7*6), n_casi, 'missing']
        except:
            n_casi = 22
            gradi = [1, int(n_casi/7*2), int(n_casi/7*3), int(n_casi/7*4), int(n_casi/7*5), int(n_casi/7*6), n_casi, 'missing']
    else:
        print "mappa" +  "errore " + nuts

    '''
    elif nuts == "nuts3":
        mappa = os.path.join(os.getcwd(), "static", "maps", "nuts3.geojson")
        try:
            gradi = [1, int(n_casi/7*2), int(n_casi/7*3), int(n_casi/7*4), int(n_casi/7*5), int(n_casi/7*6), n_casi, 'missing']
        except:
            n_casi = 22
            gradi = [1, int(n_casi/7*2), int(n_casi/7*3), int(n_casi/7*4), int(n_casi/7*5), int(n_casi/7*6), n_casi, 'missing']
    elif nuts == "nuts3_2010":
        mappa = os.path.join(os.getcwd(), "static", "maps", "nuts3_2010.geojson")
        try:
            gradi = [1, int(n_casi/7*2), int(n_casi/7*3), int(n_casi/7*4), int(n_casi/7*5), int(n_casi/7*6), n_casi, 'missing']
        except:
            n_casi = 22
            gradi = [1, int(n_casi/7*2), int(n_casi/7*3), int(n_casi/7*4), int(n_casi/7*5), int(n_casi/7*6), n_casi, 'missing']
    '''



    with open(mappa) as data_json:
        data = json.load(data_json)

    for nuts in data[u'features']:
         nuts[u'properties'].update(dataset.get(nuts[u'properties'][u'NUTS_ID'], ""))

    '''
    with open("Nuts0_KeyB.json", 'w') as data_json:
        data_json.write(json.dumps(data))
    '''
    risultato["pandas"] = dataset_pandas
    risultato["gradi"] = gradi
    risultato["json"] = data
    risultato["html"] = html

    return risultato




if __name__ == "__main__":

    dati = pd.DataFrame([[10192, 10,30], [1,20,30], [200,20,50]], columns = ["KeyB", 2012,2013])
    dati.set_index("KeyB", inplace = True)
    print map_join(dati, nuts="nuts2")#[u'features'][0]
    print map_join(dati, nuts="nuts2")["gradi"]

