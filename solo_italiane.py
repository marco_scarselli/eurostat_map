__author__ = 'SANTECH'

__author__ = 'SANTECH'




import json
import pandas as pd
import os

from estrai_ultimo_valore import estrai_ultimo_valore_serie


from sys import platform as _platform
if _platform == "linux" or _platform == "linux2":
    linux = True
elif _platform == "darwin":
    linux = True
elif _platform == "win32":
    linux = False




if linux == False:
    CodiceMap = pd.read_excel(os.path.join(os.getcwd(),"static", "KEY.xlsx"))
elif linux == True:
    CodiceMap = pd.read_excel(os.path.join(os.getcwd(),"eurostat_fvg", "static", "KEY.xlsx"))
else:
    print "errore caricamento KEY"


if __name__ == "__main__":

    '''dati = pd.DataFrame([[100, 10,30], [1,20,30], [200,20,50]], columns = ["KeyB", 2012,2013])
    dati.set_index("KeyB", inplace = True)
    print map_join(dati, nuts="nuts2")#[u'features'][0]
    print map_join(dati, nuts="nuts2")["gradi"]'''

    def checkita(x):
        if type(x) == unicode:
            if x[:2] == "IT" and len(x) == 4:
                return True
            else:
                return False
        else:
            return False
    italiane = CodiceMap[CodiceMap["CodiceMAP"].map(lambda x: checkita(x))]

    italiane.drop_duplicates("CodiceMAP")[["CodiceMAP", "Nome"]].set_index("CodiceMAP").to_json(path_or_buf= os.path.join(os.getcwd(),"static", "italia.json") , orient="index")
    #print italiane.drop_duplicates("CodiceMAP")[["CodiceMAP", "Nome"]].set_index("CodiceMAP").to_dict(orient="split")