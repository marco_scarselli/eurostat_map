# README #

Basata su python, potete provare il prototipo su [http://app.retesviluppo.it](http://app.retesviluppo.it). il sistema permette di caricare più di 6000 dataset open-data disponibili su Eurostat e di effettuare alcune elaborazioni statistiche su di essi: come output viene generata una mappa raffigurante un ranking della variabile/dataset selezionato.

### Video Guida utilizzo dell'app
[youtube](https://youtu.be/_mAUeYVQM3k)

### Licenza 

MIT