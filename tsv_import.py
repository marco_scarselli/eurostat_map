import os
import pandas as pd


def anni_int(colonne):
    nuove_colonne = []
    for item in colonne:
        try:
            nuove_colonne.append(int(item))
        except:
            nuove_colonne.append(item)
    return nuove_colonne



class TsvCleaner(object):
    def __init__(self, tsvfile):
        self.indice = pd.read_csv(tsvfile, sep=',').iloc[:,:-1]
        self.dati = pd.read_csv(tsvfile, sep='\t').iloc[:,1:]
        self.dati = self.dati.applymap(lambda x: pulisci(x))
        self.nuts = pd.read_csv(tsvfile, sep='\t').iloc[:,0]
        self.colonne = self.nuts.name.split(",")
        print self.colonne
        print self.nuts
        try:
            self.nuts = self.nuts.map(lambda x: x.split(",")[self.colonne.index('geo\\time')])
        except:
            try:
                self.nuts = self.nuts.map(lambda x: x.split(",")[self.colonne.index('post\\time')])
            except:
                try:
                    self.nuts = self.nuts.map(lambda x: x.split(",")[self.colonne.index('time\\geo')])
                except:
                    print "dati strutturati in modo non conforme"

        self.nuts.name = "geo/time"


        self.dataset = pd.concat([self.indice, self.nuts, self.dati], axis=1)
        self.dataset.columns = anni_int(self.dataset.columns)

def pulisci(numero): # andrebbe cambiata la logica della libreria gestendo le eccezioni con gli if
    ''' libreria per pulire i dati tvs - per ora solo una colonna - andrebbe applicato a tutte le celle di dati2 nella funzione tsv'''

    if type(numero) == str and numero[0] == ":":
        numero = None

    elif type(numero) == str:
        try:
            numero = float(numero)

        except:
            try:
                numero = float(numero[:-2])

            except:
                try:
                    numero = float(numero[:-3])

                except:
                    #print(numero)
                    numero = None


    else:
        return numero

    return numero

def tsv(tsvFile):
    oggettoTsv = TsvCleaner(tsvFile)
    return oggettoTsv.dataset


if __name__ == "__main__":
    prova = os.path.join(os.getcwd(), "dati tsv", "hlth_cd_acdr2.tsv")
    oggettoTsv = TsvCleaner(prova)
    print oggettoTsv.indice
    print oggettoTsv.dati
    print oggettoTsv.dataset
    print oggettoTsv.colonne.index('geo\\time')