# coding: utf-8


import datetime
import os
import gzip
from bs4 import BeautifulSoup
from unidecode import unidecode


import pandas as pd
from tsv_import import tsv
#codici = r"E:\Dropbox\Dropbox\0_DB\A_Progetti_FVG\DB_FVG\dati sdmx\sbs_r_nuts06_r2.dsd.xml"
import zipfile
import urllib





def codici_sdmx(codici):
    codici_text = open(codici, "r")
    testo = codici_text.read()
    z=BeautifulSoup(testo  , 'xml')
    codifica_eurostat = {}
    y = z.findAll("CodeList")

    try:
        for tag in  y.findAll(id="CL_FREQ"):
            tag.extract()
        for tag in  y.findAll(id="CL_OBS_STATUS"):
            tag.extract()
        for tag in  y.findAll(id="CL_TIME_FORMAT"):
            tag.extract()
    except Exception,e: print str(e)

    for element in y:

        if not element["id"] in ["CL_FREQ", "CL_OBS_STATUS", "CL_TIME_FORMAT" ]:
            codifica_eurostat[element["id"]] = {}
            #print element["id"] #variabli
            for item in element.findAll("Code"):
                for items in item.findAll("Description"):
                    if items['xml:lang'] == "en":
                        #print item["value"] #codice
                        #print items.string # descrizione
                        codifica_eurostat[element["id"]][item["value"]] = unidecode(items.string)


    return codifica_eurostat


def download_sdmx_dsd(nome_file):
    str1 = "http://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?sort=1&file=data%2F"
    str2 = nome_file
    str3 = ".sdmx.zip"
    link = str1 + str2 + str3
    
    destinazione = os.path.join(os.getcwd(), "static",  "dsd",   str2 + ".zip")
    
    urllib.urlretrieve (link, destinazione)
    zfile = zipfile.ZipFile(destinazione)
    zfile.extract(str2 + ".dsd.xml",  os.path.join(os.getcwd(),"static",  "dsd"))

def download_tsv(nome_file):
    str1 = "http://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?sort=1&file=data%2F"
    str2 = nome_file
    str3 = ".tsv.gz"
    link = str1 + str2 + str3
    #print link


    destinazione = os.path.join(os.getcwd(), "static",  "tsv",   str2 + ".gz")
    
    urllib.urlretrieve (link, destinazione)
    f = gzip.open(destinazione, 'rb')
    file_content = f.read()
    f.close()
    with open(os.path.join(os.getcwd(), "static",  "tsv",   str2 + ".tsv"), "w") as outfile:
        outfile.write(file_content)

    excel = tsv(os.path.join(os.getcwd(), "static",  "tsv",   str2 + ".tsv"))
    KEY = pd.read_excel(os.path.join(os.getcwd(), "static",  "KEY.xlsx"), index = True )
    excel = pd.merge(excel, KEY,left_on = "geo/time", right_on = "Codice_read", how='left')
    excel.to_excel(os.path.join(os.getcwd(), "static",  "excel",   str2 + ".xlsx"),index = False )
    excel.to_hdf(os.path.join(os.getcwd(), "static",  "HDF5",   str2 + ".h5"), key = str2,    index = False )
    #excel.to_csv(os.path.join(os.getcwd(), "static",  "excel",   str2 + ".csv"),encoding ="utf-8", sep = ";", na_rep ="NA" )


def trasform_excel_to_hdf():
    for item in os.listdir(os.path.join(os.getcwd(), "static",  "excel")):
        pd.read_excel(os.path.join(os.getcwd(), "static",  "excel", item)).to_hdf(os.path.join(os.getcwd(), "static",  "HDF5", item[:-5] + ".h5"), key = item[:-5], mode = "w", index = False)
        print item
        print datetime.datetime.now()



def importa_singolo(codice):
    try:
        print codice
        download_tsv(codice)
        download_sdmx_dsd(codice)
    except Exception, e:
        print codice + " :Error -" + str(e)


def importa_lista(excel):
    for item in pd.read_excel(os.path.join(excel))["File"]:
        importa_singolo(item)
        #print " "
        #print item

if __name__ == "__main__":
    #importa_lista(os.path.join(os.getcwd(), "static","dataset_fvg.xlsx"))
    trasform_excel_to_hdf()

'''
if __name__ == "__main__":
    print codici_sdmx(r"E:\Dropbox\Dropbox\0_DB\A_Progetti_FVG\DB_FVG\dati sdmx\sbs_r_nuts06_r2.dsd.xml")
'''
'''
if __name__ == "__main__":
    download_tsv("sbs_r_nuts06_r2")
'''

'''
    root = Tk()
    w = Label(root, text="inserisci il codice del dataset da scaricare")
    w.pack()
    codice = Entry(root)
    codice.pack()
    w = Label(root, text="Macro settore")
    w.pack()
    sezione = Combobox(root)
    sezione['values'] = lista_sezioni
    sezione.pack()
    w = Label(root, text="che nome vuoi dare al dataset")
    w.pack()
    dataset = Entry(root)
    dataset.pack()

    esegui = Button(root, text="importa", command = importa)
    esegui.pack()



    root.mainloop()
    '''


'''
if __name__ == "__main__":
    #togliere commento per eseguire l'aggiornamento
    excel_dataset = pd.read_excel(os.path.join(os.getcwd(),"lista_dataset.xlsx" ))

    for item in excel_dataset["File"].map(lambda x: x[:-5]).unique():
        try:
            download_sdmx_dsd(item)
        except:
            print item



    excel_dataset = pd.read_excel(os.path.join(os.getcwd(),"lista_dataset.xlsx" ))
    for item in excel_dataset["File"].map(lambda x: x[:-5]).unique():
        try:
            download_tsv(item)
        except:
            print item


'''
'''
if __name__ == "__main__":
   """ a = codici_sdmx(codici)
    print a.keys()"""
   #
   importa_singolo()
'''